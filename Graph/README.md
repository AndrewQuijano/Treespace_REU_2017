## Graph Explanation
* Katherine-list - Used in preprint. It is a non-tree based network that needs 1 node removed to be tree-based. It is spanned by 2 trees.

# TODO:
* Create new networks
* Test hypothesis. If it works, let Megan know.

## Challenges: Using repeated min-cost max flow approach FAILED.
# But, I noticed the max-flows alone seem to work just fine?
* justin_list - A binary non-tree based network. This has two paths starting and ending in an omnian. It should have 3 Spanning Trees to span this network.
* justin_list_2 - A binary non tree-based network. This has 3 paths starting and ending in an omnian.
* justin_list_3 - A binary non tree-based network. This has 1 path starting and ending in an omnian of length 5.
* justin_list_4 - A binary non tree-based network. This has 1 path starting and ending in an omnian of length 7.